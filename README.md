# Rules For Jet


# Table of contents
1. [Анализ Java и Java-подобных языков](#java)
    1. [Пример 1 (sink)](#ex1)
    2. [Пример 2 (sink)](#ex2)
    3. [Пример 3 (sink)](#ex3)
    4. [Пример 4 (sink)](#ex4)
    5. [Пример 5 (sink)](#ex5)
    6. [Пример 6 (source)](#ex6)
    7. [Пример 7 (source)](#ex7)
    8. [Пример 8 (source)](#ex8)
    9. [Пример 9 (passthrough)](#ex9)
    10. [Пример 10 (passthrough)](#ex10)
2. [Анализ деревьев](#tree)
    1. [Примеры](#exs)


## Анализ Java и Java-подобных языков <a name="java"></a>

Статический анализатор исходного кода **appScreener** для анализа Java использует байт-код. Для поиска уязвимостей в исходном коде был разработан специальный язык записи паттернов. Здесь мы не будем подробно описывать структуру данного языка, сославшись на то, что это проделано в гайде от самого Солара, но типы паттернов всё же перечислим:
* *Sink* паттерны -- "ругающиеся" правила. Их смысл простым языком можно описать следующей фразой: "Видишь такой-то аргумент такого-то метода такого-то класса -- ругайся."
* *Source* паттерны -- правила "опасных источников". Перефразируем: "Если вызван такой-то метод такого-то класса, возвращаемое значение может быть небезопасно, поэтому на него нужно повесить какой-то флаг."
* *Passthrough* паттерны -- правила "переброса флагов". Можно сказать так: "Если на какой-либо сущности висел флаг, и значение этой сущности было использовано прямо или косвенно, то флаг с этой сущности должен быть переброшен на новую сущность, которая взаимодействовала с помеченной (или сброшен)." Иными словами, это любая процедура с флагом.

Мы уделим внимание примерам записи конкретных паттернов и разбору этих примеров. Изложение будет построено таким образом, что примеры с увеличением их порядкового номера будут содержать всё меньше пояснений (в то время, как первые будут достаточно подробно прокомментированы). Сделано это для того, чтобы читающий сам задумывался о методах построения и создания тех или иных паттернов. При этом концептуально новые места (например, использование специфического синтаксиса) будут подробно разобраны.

В качестве первого примера рассмотрим (и кажется, что немного исправим незначительную опечатку) в "хрестоматийном" примере из вышеупомнятого юзергайда. Итак, пример.

### Пример 1 (Sink) <a name="ex1"></a>
Предположим, что мы хотим ругаться на использование метода `dangerousMethod` класса `org.example.DangerousClass` c `нулевым` параметром типа `int` и ещё `одним или более` параметров, если первый параметр обладает флагом `TAINTED`.
``` xml
<Rules>
    <Rule>
		<!-- Необходимо задать пльзовательское имя правила. -->
		<!-- Каждое правило содержит несколько паттернов, -->
		<!-- по которым и будет выполняться поиск уязвимостей -->
		<RuleId>MY_AWESOME_RULE</RuleId>
		<Patterns>
			<!-- Здесь начинается перечисление паттернов -->
			<!-- Необходимо задать пользовательское имя  -->
			<Pattern patternId = "myFirstPattern">
				<Categories>
					<!-- Совет, основанный на опыте: почти всегда правила для Java относятся и к Android -->
					<Category>JavaWeb</Category>
					<Category>Android</Category>
				</Categories>
				<!-- Необходимо задать уровень критичности конкретного паттерна -->
				<Severity>3</Severity>
				<!-- Начало блока описания -->
				<!-- В этом месте нам надо задать набор сущностей, на которые мы дальше будем ссылаться, -->
				<!-- то есть здесь происходит только перечисление того, что будет использовано дальше. -->
				<Definitions>
					<!-- id класса - наша "внутрення" сслыка на этот класс -->
					<Class id = "class">
						<Supers includeSelf="true">
							<Class>
								<Name>
									<Value>org.example.DangerousClass</Value>
								</Name>
							</Class>
						</Supers>
					</Class>
					<!-- id метода - наша "внутрення" сслыка на этот метод -->
					<Method id = "method">
						<!-- Так как нам нужен совершенно конкретный метод, -->
						<!-- то нет никакой необходимости использовать регулярные выражения -->
						<Name>
							<Value>dangerousMethod</Value>
						</Name>
						<Parameters>
							<!-- Здесь мы указываем, что у метода должен быть параметр типа int -->
							<Parameter>
								<Type>Int</Type>
							</Parameter>
							<!-- Здесь мы указываем, что параметров метода минимум 2 -->
							<WildCard min="1"/>
						</Parameters>
					</Method>
					<!-- id инструкции - наша "внутрення" сслыка на эту инструкцию -->
					<Instruction id = "target_invoke">
						<!-- Ссылаемся на определённый выше класс -->
						<Class>
							<Ref id = "class" reftype = "classref" />
						</Class>
						<!-- Ссылаемся на определённый выше метод -->
						<Method>
							<Ref id = "method" reftype = "methodref" />
						</Method>
						<!-- Указываем аргументы метода -->
						<Arguments>
							<!-- Указываем, что первый параметр (в нумерации с нулевого) -->
							<!-- должен быть помечен флагм -->
							<Argument>
								<Pos>1</Pos>
								<Flags>
									<Flag>TAINTED</Flag>
								</Flags>
							</Argument>
						</Arguments>
					</Instruction>
				</Definitions>
				<!-- Определяем условия, при которых возникает уязвимость -->
				<Condition>
					<!-- Просто ссылаемся на заданную выше инструкцию, -->
					<!-- которая ссылается на заданный метод конкретного класса -->
					<InstructionCondition>
						<Ref id = "target_invoke" reftype = "instructionref" />
					</InstructionCondition>
				</Condition>
				<!-- Выводим информацию о вызванной небезопасной инструкции -->
				<ReportValues>
					<ReportValue key = "bad_instruction">
						<Ref id = "target_invoke" reftype = "instructionref" />
					</ReportValue>
				</ReportValues>
			</Pattern>
		</Patterns>
	</Rule>
</Rules>
```
Что мы сделали? 
* Задали имя конкретного правила в `<RuleId>`.
* Задали имя конкретного паттерна в `<Pattern>`.
* Определили категорию (категории), к которой (которым) относится данный паттерн в `<Categories>`. **(Не правило целиком! А конкретный паттерн.)**
* Указали уровень критичности данного паттерна в `<Severity>`.
* Определили конкретный метод с конкретными аргументами конкретного класса, а также указали инструкцию (используя ссылки на класс и метод) в `<Definitions>`.
* Указали условия (используя ссылку на инструкцию), при которых возникает уязвимость в `<Condition>`.
* Вывели информацию о вызванной небезопасной инструкции в `<ReportValues>`, снова ссылаясь на инструкцию.

### Пример 2 (Sink) <a name="ex2"></a>
Теперь рассмотрим реальный паттерн.

Известно, что хеш-функции `MD2, MD5, SHA1` обладают уязвимостями. Будем ругаться в том случае, если используется метод `createPasswordHash` класса `org.jboss.security.Util`. Что нужно сделать? 

1. Посмотреть на описание класса, например, [тут](https://docs.jboss.org/jbossas/javadoc/4.0.4/security/org/jboss/security/Util.html).
2. Найти там интересующий нас метод и узнать его сигнатуру, например, [тут](https://docs.jboss.org/jbossas/javadoc/4.0.4/security/org/jboss/security/Util.html#createPasswordHash(java.lang.String,%20java.lang.String,%20java.lang.String,%20java.lang.String,%20java.lang.String)).
3. Данный метод имеет такую сигнатуру:
```java
public static String createPasswordHash(String hashAlgorithm,
                                        String hashEncoding,
                                        String hashCharset,
                                        String username,
                                        String password)
```
```
Calculate a password hash using a MessageDigest.
Parameters:
hashAlgorithm - - the MessageDigest algorithm name
hashEncoding - - either base64 or hex to specify the type of encoding the MessageDigest as a string.
hashCharset - - the charset used to create the byte[] passed to the MessageDigestfrom the password String. If null the platform default is used.
username - - ignored in default version
password - - the password string to be hashed
Returns:
the hashed string if successful, null if there is a digest exception
```
4. Из пункта 3 мы понимаем, что нас интересует нулевой аргумент, тип которого `java.lang.String`. Также мы видим, что аргументов у этого метода 5 штук.
5. Теперь у нас есть всё, чтобы написать необходимый паттерн.
``` xml
<Rule>
    <RuleId>VERY_BAD_HASH</RuleId>
    <Pattern patternId = "0001">
        <Categories>
            <Category>JavaWeb</Category>
            <Category>Android</Category>
        </Categories>
        <Severity>3</Severity>
        <Definitions>
            <Class id = "class">
                <Supers includeSelf = "True">
                    <Class>
                        <Name>
                            <Value>org.jboss.security.Util</Value>
                        </Name>
                    </Class>
                </Supers>
            </Class>
            <Method id = "method">
                <Name>
                    <Value>createPasswordHash</Value>
                </Name>
                <Parameters>
                    <Parameter>
                        <Type>java.lang.String</Type>
                    </Parameter>
                </Parameters>
            </Method>
            <Instruction id = "instruction">
                <Class>
                    <Ref id = "class" reftype = "classref" />
                </Class>
                <Method>
                    <Ref id = "method" reftype = "methodref" />
                </Method>
                <Arguments>
                    <Argument>
                        <Pos>0</Pos>
                        <!-- Здесь мы будем использовать регулярное выражение для определения имени метода, -->
                        <!-- поскольку нам подходит несколько вариантов -->
                        <Constant>
                            <ConstantString>
                                <!-- Данное регулярное выражение будет матчить значения: -->
                                <!-- [MD2, MD5, SHA, SHA1] -->
                                <Regex>(MD(2|5)|SHA(1)?)$</Regex>
                            </ConstantString>
                        </Constant>
                    </Argument>
                </Arguments>
            </Instruction>
        </Definitions>
        <Condition>
            <InstructionCondition>
                <Ref id="instruction" reftype="instructionref"/>
            </InstructionCondition>
        </Condition>
        <ReportValues>
            <ReportValue key="bad_instruction">
                <Ref id="instruction" reftype="instructionref"/>
            </ReportValue>
        </ReportValues>
    </Pattern>
</Rule>
```
Основное отличие от первого паттерна состоит в блоке названия метода: мы использовали регулярное выражение, потому что нам подходит множество имён. (Кстати, для тестирования регулярных выражений очень рекомендую [этот сайт](https://regex101.com).)

### Пример 3 (Sink) <a name="ex3"></a>
Также распространена уязвимость, связанная с использованием небезопасного алгоритма шифрования данных. Например, `javax.crypto.NullCipher` ([см. здесь](https://overcoder.net/q/1385935/насколько-безопасен-javaxcryptocipher)). В этом случае, к слову, метод является конструктором.

``` xml
<Rule>
    <RuleId>VARY_BAD_ALGO</RuleId>
    <Patterns>
        <Pattern patternId="0002">
            <Categories>
                <Category>JavaWeb</Category>
                <Category>Android</Category>
            </Categories>
            <Severity>3</Severity>
            <Definitions>
                <Class id="class">
                    <Supers includeSelf="true">
                        <Class>
                            <Name>
                                <Value>javax.crypto.NullCipher</Value>
                            </Name>
                        </Class>
                    </Supers>
                </Class>
                <!-- Метод являеется конструктором, поэтому ипользуется специальный тег -->
                <Method id="method">
                    <Constructor/>
                </Method>
                <Instruction id="instruction">
                    <Class>
                        <Ref id="class" reftype="classref"/>
                    </Class>
                    <Method>
                        <Ref id="method" reftype="methodref"/>
                    </Method>
                </Instruction>
            </Definitions>
            <Condition>
                <InstructionCondition>
                    <Ref id="instruction" reftype="instructionref"/>
                </InstructionCondition>
            </Condition>
            <ReportValues>
                <ReportValue key="bad_instruction">
                    <Ref id="instruction" reftype="instructionref"/>
                </ReportValue>
            </ReportValues>
        </Pattern>
    </Patterns>
</Rule>
```
Отличие от предыдущих случаев состоит в том, что вызываемый метод является конструктором, поэтому и был использован специальный тег `<Constructor />`.

### Пример 4 (Sink) <a name="ex4"></a>
SQL-инъекции, одна из самых популярных уязвимостей в мире. Иногда с целью снижения рисков используются подготовленные запросы. Но и это может быть опасно в том случае, если аргумент метода будет заражен ([см. здесь](https://github.com/Code-Racing/brickyard/issues/42)). Будем ругаться на метод `newPreparedStatementCreator` класса `org.springframework.jdbc.core.PreparedStatementCreatorFactory` в том случае, если нулевой аргумент, из двух параметров типа `String` и `Object[]`, отмечен флагом `TAINTED` или `DETNIAT`, при этом **НЕ** отмечен флагом `NOT_TAINTED`. ([Ссылка](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/jdbc/core/PreparedStatementCreatorFactory.html#newPreparedStatementCreator-java.lang.Object:A-) на описание метода.)

``` xml
<Rule>
    <RuleId>VERY_BAD_SQL</RuleId>
    <Patterns>
        <Pattern patternId="0003">
            <Categories>
                <Category>JavaWeb</Category>
            </Categories>
            <Severity>3</Severity>
            <Definitions>
                <Class id="class">
                    <Supers>
                        <Class>
                            <Name>
                                <Value>org.springframework.jdbc.core.PreparedStatementCreatorFactory</Value>
                            </Name>
                        </Class>
                    </Supers>
                </Class>
                <Method id="method">
                    <Name>
                        <Value>newPreparedStatementCreator</Value>
                    </Name>
                    <Parameters>
                        <Parameter>
                            <Type>java.lang.String</Type>
                        </Parameter>
                        <Parameter>
                            <Type>java.lang.Object[]</Type>
                        </Parameter>
                    </Parameters>
                </Method>
                <Instruction id="instruction">
                    <Class>
                        <Ref id="class" reftype="classref"/>
                    </Class>
                    <Method>
                        <Ref id="method" reftype="methodref"/>
                    </Method>
                    <Arguments>
                        <Argument>
                            <Pos>0</Pos>
                            <Flags>
                                <Or>
                                    <Flag>TAINTED</Flag>
                                    <Flag>DETNIAT</Flag>
                                </Or>
                                <Not>
                                    <Flag>NOT_TAINTED</Flag>
                                </Not>
                            </Flags>
                        </Argument>
                    </Arguments>
                </Instruction>
            </Definitions>
            <Condition>
                <InstructionCondition>
                    <Ref id="instruction" reftype="instructionref"/>
                </InstructionCondition>
            </Condition>
            <ReportValues>
                <ReportValue key="bad_instruction">
                    <Ref id="instruction" reftype="instructionref"/>
                </ReportValue>
            </ReportValues>
        </Pattern>
    </Patterns>
</Rule>
```
### Пример 5 (Sink) <a name="ex5"></a>
Известно, что использование пустого пароля небезопасно. Представим, что мы используем метод `changePassword` класса `org.owasp.esapi.Authenticator` ([описание класса](https://www.javadoc.io/doc/org.owasp.esapi/esapi/2.0.1/org/owasp/esapi/Authenticator.html)), но в качестве нового пароля задаём пустое значение. Будем ругаться на вызов такой конструкции.

``` xml
<Rule>
    <RuleId>VERY_EMPTY_PASSWORD</RuleId>
    <Patterns>
        <Pattern patternId="0004">
            <Categories>
                <Category>JavaWeb</Category>
            </Categories>
            <Severity>3</Severity>
            <Definitions>
                <Class id="class">
                    <Supers includeSelf="true">
                        <Class>
                            <Name>
                                <Value>org.owasp.esapi.Authenticator</Value>
                            </Name>
                        </Class>
                    </Supers>
                </Class>
                <Method id="method">
                    <Name>
                        <Value>changePassword</Value>
                    </Name>
                    <!-- Указываем, что число параметров метода в точности равно 4 -->
                    <Parameters>
                        <WildCard num="4"/>
                    </Parameters>
                </Method>
                <Instruction id="instruction">
                    <Class>
                        <Ref id="class" reftype="classref"/>
                    </Class>
                    <Method>
                        <Ref id="method" reftype="methodref"/>
                    </Method>
                    <Arguments>
                        <Argument>
                            <!-- Отмечаем, что на 3 позиции аргумент принимает пустое значение -->
                            <Pos>3</Pos>
                            <Constant>
                                <ConstantString>
                                    <Value/>
                                </ConstantString>
                            </Constant>
                        </Argument>
                    </Arguments>
                </Instruction>
            </Definitions>
            <Condition>
                <InstructionCondition>
                    <Ref id="instruction" reftype="instructionref"/>
                </InstructionCondition>
            </Condition>
            <ReportValues>
                <ReportValue key="bad_instruction">
                    <Ref id="instruction" reftype="instructionref"/>
                </ReportValue>
            </ReportValues>
        </Pattern>
    </Patterns>
</Rule>
```
### Пример 6 (Source) <a name="ex6"></a>
Теперь перейдем к созданию source-паттернов.

Предположим, что на возвращаемое значение метода `getFlag` класса `test.class.NeedFlag` мы хотим добавить флаг `TAINTED`. Логика записи паттерна ровно такая же, как и во всех предыдущих случаях, за тем лишь исключением, что нам надо выполнить ни `<ReportValues>`, а `<ChangeFlags>`.

``` xml
<Rule>
    <RuleId>FIRST_SOURCE</RuleId>
    <Patterns>
        <Pattern patternId="0005">
            <Categories>
                <Category>JavaWeb</Category>
            </Categories>
            <Definitions>
                <Class id="class">
                    <Supers includeSelf="true">
                        <Class>
                            <Name>
                                <Value>test.class.NeedFlag</Value>
                            </Name>
                        </Class>
                    </Supers>
                </Class>
                <Method id="method">
                    <Name>
                        <Regex>getFlag</Regex>
                    </Name>
                </Method>
                <Instruction id="instruction">
                    <Class>
                        <Ref id="class" reftype="classref"/>
                    </Class>
                    <Method>
                        <Ref id="method" reftype="methodref"/>
                    </Method>
                </Instruction>
            </Definitions>
            <Condition>
                <InstructionCondition>
                    <Ref id="instruction" reftype="instructionref"/>
                </InstructionCondition>
            </Condition>
            <!-- Теги изменения флагов -->
            <ChangeFlags>
                <!-- Тег установки флага на возвращаемое значение вызываемого метода -->
                <FlagsSet>
                    <To>
                        <Argument>
                            <Return/>
                        </Argument>
                    </To>
                    <!-- Само значение устанавливаемого флага -->
                    <FlagsDiff>+TAINTED</FlagsDiff>
                </FlagsSet>
            </ChangeFlags>
        </Pattern>
    </Patterns>
</Rule>
```

### Пример 7 (Source) <a name="ex7"></a>

Предположим, что на метод `getValue` класса `javax.faces.component.ValueHolder` мы хотим добавить флаг `TAINTED`.

``` xml
<Rule>
    <RuleId>SECOND_SOURCE</RuleId>
    <Patterns>
        <Pattern patternId="0006">
            <Categories>
                <Category>JavaWeb</Category>
            </Categories>
            <Definitions>
                <Class id="class">
                    <Supers includeSelf="true">
                        <Class>
                            <Name>
                                <Value>javax.faces.component.ValueHolder</Value>
                            </Name>
                        </Class>
                    </Supers>
                </Class>
                <Method id="method">
                    <Name>
                        <Value>getValue</Value>
                    </Name>
                </Method>
                <Instruction id="instruction">
                    <Class>
                        <Ref id="class" reftype="classref"/>
                    </Class>
                    <Method>
                        <Ref id="method" reftype="methodref"/>
                    </Method>
                </Instruction>
            </Definitions>
            <Condition>
                <InstructionCondition>
                    <Ref id="instruction" reftype="instructionref"/>
                </InstructionCondition>
            </Condition>
            <ChangeFlags>
                <FlagsSet>
                    <To>
                        <Argument>
                            <Return/>
                        </Argument>
                    </To>
                    <FlagsDiff>+TAINTED</FlagsDiff>
                </FlagsSet>
            </ChangeFlags>
        </Pattern>
    </Patterns>
</Rule>
```

### Пример 8 (Source) <a name="ex8"></a>

Предположим, что на возвращаемое значение метода `copyToByteArray` класса `org.springframework.util.FileCopyUtils`, аргумент которого является типа `java.io.InputStream`, нужно добавить два флага `TAINTED` и `DETNIAT` ([описание метода](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/util/FileCopyUtils.html#copyToByteArray-java.io.InputStream-)).

``` xml
<Rule>
    <RuleId>THIRD_SOURCE</RuleId>
    <Patterns>
        <Pattern patternId="0008">
            <Categories>
                <Category>JavaWeb</Category>
            </Categories>
            <Definitions>
                <Class id="class">
                    <Supers includeSelf="true">
                        <Class>
                            <Name>
                                <Value>org.springframework.util.FileCopyUtils</Value>
                            </Name>
                        </Class>
                    </Supers>
                </Class>
                <Method id="method">
                    <Name>
                        <Value>copyToByteArray</Value>
                    </Name>
                    <Parameters>
                        <Parameter>
                            <Type>java.io.InputStream</Type>
                        </Parameter>
                    </Parameters>
                </Method>
                <Instruction id="instruction">
                    <Class>
                        <Ref id="class" reftype="classref"/>
                    </Class>
                    <Method>
                        <Ref id="method" reftype="methodref"/>
                    </Method>
                </Instruction>
            </Definitions>
            <Condition>
                <InstructionCondition>
                    <Ref id="instruction" reftype="instructionref"/>
                </InstructionCondition>
            </Condition>
            <ChangeFlags>
                <FlagsSet>
                    <To>
                        <Argument>
                            <Return/>
                        </Argument>
                    </To>
                    <FlagsDiff>+TAINTED,+DETNIAT</FlagsDiff>
                </FlagsSet>
            </ChangeFlags>
        </Pattern>
    </Patterns>
</Rule>
```

### Пример 9 (Passthrough) <a name="ex9"></a>

В заключение рассмотрим два паттерна для перевешивания флагов. Предположим, что мы хотим "перевесить" флаг с `нулевого` аргумента одного из двух методов: `foo_method` или `bar_method` класса `test.Class` на `this` этого класса.

``` xml
<Rule>
    <RuleId>FIRST_PASSTHROUGH</RuleId>
    <Patterns>
        <Pattern patternId="0009">
            <Categories>
                <Category>JavaWeb</Category>
            </Categories>
            <!-- Аналогичным образом определяем класс и метод -->
            <Definitions>
                <Class id="class">
                    <Supers includeSelf="true">
                        <Class>
                            <Name>
                                <Value>test.Class</Value>
                            </Name>
                        </Class>
                    </Supers>
                </Class>
                <Method id="method">
                    <!-- Нам подходят несколько методов, поэтому используем регулярное выражение -->
                    <Name>
                        <Regex>foo_method|bar_method</Regex>
                    </Name>
                    <Parameters>
                        <WildCard min="1"/>
                    </Parameters>
                </Method>
                <Instruction id="instruction">
                    <Class>
                        <Ref id="class" reftype="classref"/>
                    </Class>
                    <Method>
                        <Ref id="method" reftype="methodref"/>
                    </Method>
                    <Arguments>
                        <Argument id="arg0">
                            <Pos>0</Pos>
                        </Argument>
                    </Arguments>
                </Instruction>
            </Definitions>
            <Condition>
                <InstructionCondition>
                    <Ref id="instruction" reftype="instructionref"/>
                </InstructionCondition>
            </Condition>
            <!-- Начинаем перебрасывать флаги -->
            <ChangeFlags>
                <FlagsSet>
                    <!-- Устанавливаем НА this ...-->
                    <To>
                        <Argument>
                            <This/>
                        </Argument>
                    </To>
                    <!-- ... флаг, который сняли с первого аругмента -->
                    <From>
                        <Argument>
                            <Ref id="arg0" reftype="argumentref"/>
                        </Argument>
                    </From>
                </FlagsSet>
            </ChangeFlags>
        </Pattern>
    </Patterns>
</Rule>
```
### Пример 10 (Passthrough) <a name="ex10"></a>

Предположим, что в классах `my.Class` и `my.Klass` есть метод `myMethod`, параметр которого имеет тип `myParameter`. Я хочу перевесить флаг с нулевого аргумента и на `this` и на возвращаемое значение.

``` xml
<Rule>
    <RuleId>SECOND_PASSTHROUGH</RuleId>
    <Patterns>
        <Pattern patternId="0010">
            <Categories>
                <Category>JavaWeb</Category>
            </Categories>
            <Definitions>
                <Class id="class">
                    <Supers includeSelf="true">
                        <!-- Имя класса может быть одного из двух значений -->
                        <Class>
                            <Name>
                                <Regex>my.(Class|Klass)</Regex>
                            </Name>
                        </Class>
                    </Supers>
                </Class>
                <Method id="method">
                    <Name>
                        <Value>myMethod</Value>
                    </Name>
                    <Parameters>
                        <Parameter>
                            <Type>myParameter</Type>
                        </Parameter>
                    </Parameters>
                </Method>
                <Instruction id="instruction">
                    <Class>
                        <Ref id="class" reftype="classref"/>
                    </Class>
                    <Method>
                        <Ref id="method" reftype="methodref"/>
                    </Method>
                    <Arguments>
                        <Argument id="arg0">
                            <Pos>0</Pos>
                        </Argument>
                    </Arguments>
                </Instruction>
            </Definitions>
            <Condition>
                <InstructionCondition>
                    <Ref id="instruction" reftype="instructionref"/>
                </InstructionCondition>
            </Condition>
            <ChangeFlags>
                <!-- Перевешиваем флаг и на This, и на Return -->
                <FlagsSet>
                    <To>
                        <Argument>
                            <This/>
                        </Argument>
                        <Argument>
                            <Return/>
                        </Argument>
                    </To>
                    <From>
                        <Argument>
                            <Ref id="arg0" reftype="argumentref"/>
                        </Argument>
                    </From>
                </FlagsSet>
            </ChangeFlags>
        </Pattern>
    </Patterns>
</Rule>
```

## Анализ деревьев <a name="tree"></a>

Данный тип анализа отличается тем, что условия для того ругаться или нет, мы определяем из явного задания **пути** к уязвимой конструкции. Я долго думал, как лучше описать этот блок, но боюсь, что это именно тот случай, когда объяснить будет легче, чем всё это записать. В данном репозитории я привожу примеры написанных реальных паттернов для поиска уязвимостей. 

ANTLR4 [repo](https://github.com/antlr/antlr4/blob/master/doc/index.md).
Grammar [repo](https://github.com/antlr/grammars-v4).

Но всё-таки попробуем выполнить концепцию. Путь задаётся в тегах `<XPath>`, где основными элементами являются следующие сущности:
* `//` -- переход к любому заданному узлу, **НЕ** следующему.
* `/` -- переход только к следующему узлу.
* `//node1[node2][node3]` -- переход к узлу `node`, у которого есть два потомка `node1`, `node2`.
* `//node1[node2//node3]` -- переход у узлу `node`, у которого ест потомок `node2`, у которого среди потомков есть `node3`.
* `//node1[mathces(text()[1], 'foo', 'i')]` -- поиск у узла `node1` текстового значения на месте первого аргумента (нумерация с 1) текстового значения `foo`, без учёта регистра (о чём нам сигнализирует идентификатор `i`).
* `//node1[node2[string-length(text()[1])&lt;5]]` -- поиск узла `node1`, у которого есть потомок `node2`, длинна текстового значения которого меньше `5`. 
* Что касается поиска аргумента конкретного типа (`int, string` и т. д.) нужно будет понять, как в грамматике данного языка данный тип определяется, поэтому унифицированного рецепта для поиска таких правил не существует.

Может быть полезна [эта](https://www.w3schools.com/xml/xpath_axes.asp) ссылка.

### Примеры <a name="exs"></a>
Ниже привожу обещанный пример с разбором уязвимостей и реальных паттернов для их поиска.
``` xml
<!-- Попробуем создать правило для JS-->
<Rules>
    <Rule>
        <!-- Снова задаём пользовательское имя файла -->
        <RuleId>JS_PWD_EMPTY</RuleId>
        <Patterns>
            <!-- Паттерн срабатывает для -->
            <!-- var password_ = ''; -->
            <Pattern patternId="0001">
                <!-- Насколько я помню, данные паттерны также имели -->
                <!-- тег <Categories>, но в юзер-гайде этого почему-то нет. -->
                <Categories>
                    <Category>JS</Category>
                </Categories>
                <Severity>3</Severity>
                <!-- Здесь начинается непосредственно анализ дерева разбора. -->
                <!-- 1. Мы доходим до узла variableDeclaration-->
                <!-- 2. В первых квадратных скобках мы ищем у узла variableDeclaration-->
                <!-- потомка, имя которого задаётся текстом matches(text()[1], 'regex'),-->
                <!-- при этом имя не совпадает с указнным во втором аргументе-->
                <!-- (приведено для примера). -->
                <!-- Флаг 'i' означает то, что поиск происходит без учёта регистра -->
                <!-- 3. Затем мы во вторых квадратных скобках ищем условие пустоты пароля. -->
                <XPath>
                    //variableDeclaration[matches(text()[1],'.*pass(wd|word).*','i') and not(matches(text()[1],'^smth$','i'))][singleExpression/literal[not(.//text()[1])]]
                </XPath>
            </Pattern>
        </Patterns>
    </Rule>
    <Rule>
        <!-- Снова задаём пользовательское имя файла -->
        <RuleId>JS_PWD_HARDCODED</RuleId>
        <Patterns>
            <!-- Паттерн срабатывает для -->
            <!-- password > 'smth'; -->
            <!-- password != 'smth'; -->
            <Pattern patternId="0002">
                <Categories>
                    <Category>JS</Category>
                </Categories>
                <Severity>3</Severity>
                <!-- Здесь начинается непосредственно анализ дерева разбора. -->
                <!-- Здесь начинается непосредственно анализ дерева разбора. -->
                <!-- 1. Мы доходим до узла singleExpression-->
                <!-- 2. В первых квадратных скобках мы ищем у узла singleExpression-->
                <!-- первого потомка. Далее у него мы ищем среди всех потомков явно пароль, используя регекс-->
                <!-- 3. Далее мы ищем специальный символ в блоке [matches(text()[1],'[!=&gt;&lt;]=') or matches(text()[1],'[&lt;&gt;]')]-->
                <!-- 4. В заключение у второго singleExpression[2] мы ищем предка literal, -->
                <!-- У которого в свою очередь ищем отсутсвие одного из следующих значений ^null|true|false$ -->
                <XPath>
                    //singleExpression[singleExpression[1]//text()[1][matches(.,'^pass(wd|word)$','i')]][matches(text()[1],'[!=&gt;&lt;]=') or matches(text()[1],'[&lt;&gt;]')][singleExpression[2]/literal[text()[1][not (matches(.,'^null|true|false$','i'))]]]
                </XPath>
            </Pattern>
        </Patterns>
    </Rule>
    <Rule>
        <RuleId>JS_RNDM</RuleId>
        <Patterns>
            <Pattern patternId="0003">
                <Categories>
                    <Category>JS</Category>
                </Categories>
                <Severity>3</Severity>
                <!-- Доходим до узла singleExpression, у него ищем потомка singleExpression, -->
                <!-- у которого ищем потомка singleExpression, значение которого совпадает с Math,-->
                <!-- а также потомка identifierName, значение которого random. -->
                <XPath>
                    //singleExpression[singleExpression[singleExpression[matches(text()[1],'^(Math)$')]][identifierName[matches(text()[1],'^(random)$','i')]]
                </XPath>
            </Pattern>
        </Patterns>
    </Rule>
    <Rule>
        <!-- Срабатывает для -->
        <!-- СтрокаПодключения = "Srvr=""*****"";Ref=""*****"";Usr=""ExData"";Pwd=NULL;" -->
        <RuleId>ONES_PWD</RuleId>
        <Patterns>
            <Pattern patternId="0004">
                <Categories>
                    <Category>1C</Category>
                </Categories>
                <Severity>3</Severity>
                <!-- Здесь мы доходим до узла  stringLiteral, далее у него находим потомка stringPart-->
                <!-- Далее мы ищем у ула StringPart потомка, значение которого матчится с текстом строки, -->
                <!-- связанной с пустым паролем -->
                <XPath>//stringLiteral/stringPart[matches(text()[1], 'Pwd=NULL', 'i')]</XPath>
            </Pattern>
        </Patterns>
    </Rule>
    <Rule>
        <!-- Срабатывает для -->
        <!-- DBMS_CRYPTO.Hash(str, HASH_MD4); -->
        <RuleId>PSQL_HASH</RuleId>
        <Patterns>
            <Pattern patternId="0005">
                <Categories>
                    <Category>PLSQL</Category>
                </Categories>
                <Severity>3</Severity>
                <!-- Здесь мы доходим до узла  function_call, далее у него находим потомка routine_name.-->
                <!-- Далее мы потомков у routine_name. -->
                <!-- Далье мы переходим к следующему потомку function_call - function_argument,-->
                <!-- а дальше уже находим инетерсующий нас небезопасный метод вызова HASH_MD4-->
                <XPath>//function_call[routine_name[id/id_expression/regular_id[matches(text()[1],'^DBMS_CRYPTO$','i')]][id_expression/regular_id[matches(text()[1],'^HASH$','i')]]][function_argument/argument[2][.//general_element_part/id_expression/regular_id[matches(text()[1],'^HASH_MD4$','i')]]]</XPath>
            </Pattern>
        </Patterns>
    </Rule>
</Rules>
```
